[![Docker](https://badgen.net/badge/icon/DockerHub?icon=docker&label)](https://hub.docker.com/r/xelofan/allthemods6)
[![GitLab last commit](https://badgen.net/gitlab/last-commit/Xelofan/all-the-mods-6-docker/)](https://gitlab.com/Xelofan/all-the-mods-6-docker/-/commits)
# !!! READ BEFORE USE !!!

Download the latest All the Mods 6 server pack from [here](https://www.curseforge.com/minecraft/modpacks/all-the-mods-6), extract it to a folder, then mount it to the container pointing to /data.

To change the memory assigned to the server, modify the **maxRam** and **minRam** properties inside the server-setup-config.yaml file.

# Docker Compose example:
```yaml
version: '3'

services:
  atm6:
    image: registry.gitlab.com/xelofan/all-the-mods-6-docker
    container_name: atm6
    restart: on-failure
    volumes:
      - ./atm6:/data
    ports:
      - 25565:25565
```

# Docker CLI example:
```
docker run --name atm6 --restart on-failure -v $(pwd)/atm6:/data -p 25565:25565 registry.gitlab.com/xelofan/all-the-mods-6-docker
```
