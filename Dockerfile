FROM eclipse-temurin:8-jdk

RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive \
  apt-get install -y \
    imagemagick \
    gosu \
    sudo \
    net-tools \
    iputils-ping \
    curl wget \
    git \
    jq \
#    dos2unix \
    mysql-client \
    tzdata \
    rsync \
    nano \
    unzip \
    knockd \
    ttf-dejavu \
    && apt-get clean

EXPOSE 25565 25575

ARG TARGETOS
ARG TARGETARCH
ARG TARGETVARIANT

STOPSIGNAL SIGTERM

VOLUME [ "/data" ]
WORKDIR /data

ENTRYPOINT [ "bash", "startserver.sh" ]